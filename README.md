# CMamboS

Este es el CMamboS, el único CMS que le pone ritmo y sabor a tu aplicación.

## Requerimientos

* [Composer](https://getcomposer.org/)
* [Laravel](http://laravel.com/) (> v5.0.1)
* Configurar los accesos a la base de datos en tu aplicarión laravel

## Instalación


1. Agrega lo siguiente a tu archivo `composer.json`

		"repositories": [
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cmambos.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-core.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-dashboard.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-site.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-administrator.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-user.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-page.git"
			},
			{
				"type": "vcs",
				"url": "git@bitbucket.org:compec/cms-components.git"
			},
            {
                "type": "vcs",
                "url": "git@bitbucket.org:compec/cms-menu-manager.git"
            },
            {
                "type": "vcs",
                "url": "git@bitbucket.org:compec/cms-form-register.git"
            }
		],

2. Agrega lo siguiente a la llave "require" de tu `composer.json`:

		"mambo/cms-bootstrap": "dev-master"

3. Agrega lo siguiente a tu `composer.json`:

		"minimum-stability": "dev"

4. Ejecuta `composer update`.

5. Agrega lo siguiente al final del arreglo `providers` de tu archivo `config/app.php`:

		'Mambo\Cms\Bootstrap\Providers\CmsServiceProvider',

6. Publica los archivos necesarios:

		php artisan vendor:publish

7. Ejecuta `composer dumpautoload`.

8. Ejecuta `cmambos:install`.
