<?php namespace Mambo\Cms\Bootstrap\Providers;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->register('Mambo\Cms\Core\Providers\CommandServiceProvider');
		$this->app->register('Mambo\Cms\Core\Providers\ValidatorServiceProvider');
		$this->app->register('Mambo\Cms\Dashboard\Providers\DashboardServiceProvider');
		$this->app->register('Mambo\Cms\Site\Providers\SiteServiceProvider');
		$this->app->register('Mambo\Cms\Administrator\Providers\AdministratorServiceProvider');
		$this->app->register('Mambo\Cms\User\Providers\UserServiceProvider');
		$this->app->register('Mambo\Cms\Page\Providers\PageServiceProvider');
		$this->app->register('Mambo\Cms\Components\Providers\ComponentsServiceProvider');
        $this->app->register('Mambo\Cms\Menu\Providers\MenuServiceProvider');
        $this->app->register('Mambo\Cms\FormRegister\Providers\FormRegisterServiceProvider');
	}
}
